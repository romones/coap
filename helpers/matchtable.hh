#ifndef MATCHTABLE_H
#define MATCHTABLE_H

#include <iostream>
#include <list>
#include <map>
#include <string>
#include <vector>
#include <ctime>
#include <doors/frame.h>
#include <doors/timer.h>
//Uints?
#include <doors/core.h>
#include "options.hh"
#include "../impl/coappeer.hh"

class Matchtable 
{
public:
  
  Matchtable(int size,);
  ~Matchtable();
  
  //true if msg is added to the table
  bool searchtable(Coappeer::CMSG* msg) const;

  //checktable tells if the given message is relevant for us and 
  //calls forth any action
  bool checktable(Coappeer::CMSG* msg) const;
  
  //add() puts msg into Matchtable,
  //bool must be true when our node is the source and false when someone else is
  bool add(Coappeer::CMSG* msg, bool src);
  //extends timestamp value with rt_timer and increments retransmitctr
  void cleartimer(Coappeer::CMSG* msg);
  
  
private:
  
  Timer Mtimer_;
  unsigned int Listsize_;
  //Current_ is iterator to place in Entries where we add next 
  list<Entry_>::iterator Current_;
  
  enum Statustype
    {
      WAIT_ACK, READY, WAIT_RSP, DEAD
    };
  
  enum Matchtype
    {
      TOKENMATCH, IDMATCH, NONE
    };
  
  
  struct Entry_
  {
    Uint8 type;
    Uint8 code;
    Uint16 messageid;
    Option token;//opt
    InetAddr targetaddress;//IP and PORT
    std::string uripath;//opt
    int retransmitctr;
    Statustype status;
    //if message is ACK. We put pointer onto the confirmable message.
    Entry_* ackto;
    //Timestamp tells when to retransmit
    time_t timestamp;
    Coappeer::CMSG whole; //should we just store the msg
  };
  
  std::multimap<Uint16,*Entry_> IDmap;
  
  std::list<Entry> Entries;
  
  //private helper for checktable()
  Matchtype checkentry(Entry* matcher, Entry* target);
  
  Entry_ stripmessage(Coappeer::CMSG* msg);

  //
  void killzombies();
  
  //no reason to copy or set this table anywhere so we ban such behaviour
  Matchtable(const Matchtable& copy)
  Matchtable& operator=(Matchtable & set)
}
  
#endif
  
