#include "matchtable.hh"

Matchtable::Matchtable(int size):
{
  Mtimer_(this,OTime(rt_timer));
  Listsize_ = size;
  //Mtimer_start();
}

Matchtable::~Matchtable()
{
}

bool Matchtable::searchtable(Coappeer::CMSG* msg) const
{
  Entry temp = stripmessage(msg);
  
  std::map<Uint16, *Entry>::iterator i;
  
  i = IDmap.find(temp.messageID);
  
  if (i != IDmap.end())
    {
      return true;
    }
  else
    {
      return false;
     }
  
  
}

/*void killzombies()
{
   list<Entry*>::iterator cursor = Current_;
   if ( cursor->status == DEAD )
     {
	
     }
   
}*/


bool Matchtable::checktable(Coappeer::CMSG* msg) const
{
   list<Entry>::iterator followup = Entries.begin();       
   killzombies();
   while(followup != Enties.end())
     {
	/*if (followup == Entries.end())
	  {
	     followup == Entries.begin();
	  }
	*/

	if (followup.status != DEAD &&
	    time(NULL) - followup.timestamp > followup.agelimit )
	  {
	     wake_retransmission(followup->whole);
	  }
	followup++;
     }
   
}

bool Matchtable::add(Coappeer::CMSG* msg, bool src)
{
   Entry_ tmp = stripmessage(msg);

   if(!src && tmp.targetaddress != own)
     {
       std::cout << "wont add to Matchtable: msg was not for us" ;
       std::cout << std::endl;
       return false;
     }

   switch (tmp.type)
     {
       //ok is true if message is associated with some previous message and 
       //it can still be found
        bool ok = false;
        list<Entry_>::iterator it = Entries.begin();
        
      case 0:
        for(it; it < Entries.end(); ++it)
        {
           if(it.status != DEAD)
           {
              if (checkentry(&*it,&tmp) == TOKENMATCH)
              {
                 it.status = READY;
              }
              
           }
           
        }
   
        //search for same token & WAIT_RSP
        
	tmp.status = WAIT_ACK;
	ok = true;
      case 1:
	tmp.status = READY;
	ok =  true;
      case 2:
	tmp.status = READY;
	//we have to look for messages with the same messageid and set
	//them to either READY or WAIT_RSP
	for(it; it < Entries.end(); ++it)
	  {
	    if(it.status != DEAD)
	      { 
		//when IDs match and CON is followed by ACK
		if(checkentry(&*it,&tmp) == IDMATCH
		   && it.type == 0)
		  {
		    ok = true;
		    //if piggypacked then READY
		    if(tmp.code > 63 && tmp.code < 192 ) 
		      {
			it.status = READY;
		      }
		    else if(tmp.code == 0)
		      {
			it.status = WAIT_RSP;
		      }
		    else
		      {
			std::cout << "wont add to Matchtable: inconsistent code" ;
			std::cout << std::endl;
			return false;
		      }
		  }
	      }
	  }
	
     case 3: //reset
       tmp.status = READY;
       //we have to look for messages with the same messageid and set
       //them to DEAD
       for(it; it < Entries.end(); ++it)
	 {
	   if(it.status != DEAD)
	     { 
	       //when IDs match and foud msg is either CON or NON
	       if(checkentry(&*it,&tmp) == IDMATCH
		  && it.type < 2)
		 {
		   ok = true;
		   it.status = DEAD ;
		 }
	     }
	 }
     }
   if(!ok)
     {
       std::cout << "wont add to Matchtable: predecessor not found" ;
       std::cout << std::endl;
       return false;
     }
   
   if (Entries.size() < Listsize_)
     {
       Entries.push_back(tmp);
       IDmap.insert(std::pair<Uint16,Entry_>(tmp.messageid,*Entries.back));
       Current_ = Entries.end()
       return true;
     }
   
   if (Entries.size() >= Listsize_)
     {
       Current_ = Entries.begin();
     }
   else
     {
       Entries.insert(Current_,tmp);
       Current_ = Entries.erase(Current_);
       if(Current_ >= Entries.end())
	 {
	   Current_ = Entries.begin();
	 }
     }
   return true;
}

void Matchtable::cleartimer(Coappeer::CMSG* msg)
{
  list<Entry_>::iterator it = Entries.begin();
  for(it; it < Entries.end(); ++it)
    {
      if(it.status != DEAD && it.whole == msg) //check this through
	{ 
	  it.timestamp += (rt_timer*(2^(++it.retransmitctr)));
	  if(it.retransmitctr >= rt_counter)
	    {
	      it.status = DEAD;
	    }
	}
    }
  return;
}

void Matchtable::setsize(unsigned int size)
{
  Listsize_ = size;
  return;
}

//private helper for checktable()
Matchtype Matchtable::checkentry(Entry* matcher, Entry* target) const
{
  if( matcher.messageID == target.messageID)
    {
      if(matcher.targetip == target.targetip && matcher.port == 
	 target.port)
	  {
	    // check if other stuff needs to be matched. is IP matched like
	    // this... think :D
	    return IDMATCH;
	  }
	
    }
  
  
  else if( matcher.token == target.token)
    {
      return TOKENMATCH;
     }
  return NONE;
}

Entry_ Matchtable::stripmessage(Coappeer::CMSG* msg)
{
   Entry_ temp;
   
   //default values
   temp.type = 0;
   temp.code = 0;
   temp.messageid = 0;
   temp.token = {0,0};
   temp.targetaddress = InetAddr(0,0);
   temp.uripath = "";
   temp.retransmitctr = 0;
   temp.status = DEAD;
   temp.ackto = NULL;
   temp.timestamp = (time(0)+rt_timer);
   temp.whole = msg;
   temp.retransmitctr = 0 ;
   
   Frame hdr = ((Coappeer::CSMG*) msg)->Header;
   HDRType* headers = new HDRType;
   headers = unpack_hdr(&hdr);
   
   temp.type = headers.type;
   temp.code = headers.code;
   temp.messageID = headers.id;
   
   Frame opts = ((Coappeer::CSMG*) msg)->Options;
   vector<Option>* temp2;
   int count = temp.opcount;
   temp2 = unpack_opts(&opts, count);
   
   
   unsigned int deltasum = 0;
   for (int i = 0; i < count; i++)
   {
      deltasum =+ temp2.at(i)->header.delta;
      
      if (deltasum == 5)
      {
      temp.targetip = temp2.at(i)->value;
      }
      if (deltasum == 11)
      {
	 temp.token = temp2.at(i)->value;
      }
      if (deltasum == 7)
      {
	 temp.port = temp2.at(i)->value;
      }
      if (deltasum == 9)
      {
      temp.uripath = temp2.at(i)->value;
      }
   }
   return temp; 
}

void Matchtable::killzombies()
{
  list<Entry_>::iterator it = Entries.begin();
  for(it; it < Entries.end(); ++it)
    {
      if(it.status == DEAD || it.retransmitctr >= rt_counter)
	{
	  multimap<Uint16,*Entry_>::iterator yt;
	  while(yt = IDmap.find(it.messageid) != IDmap.end())
	    {
	      IDmap.erase(yt);
	    }
	  if(it.type == 2)
	    {
	      Entries.erase(it.ackto);
	    }
	  Entries.erase(it);
	}
    }
  return;
}
