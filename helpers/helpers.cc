#include "helpers.hh"

HDRType* unpack_hdr(Frame *frame){

   HDRType *hdr = new HDRType;
   
    for (int i=0;i<4;i++){
      (*hdr).block[i] = frame->read(i);
    }

hdr->bytes = htonl(hdr->bytes);
 
 return hdr;
}

Frame* pack_hdr(HDRType *hdr){

Frame *frame = new Frame;

hdr->bytes = htonl(hdr->bytes);
std::cout << std::hex << hdr->bytes << std::endl;
  for (int i=0;i<4;i++){
    std::cout << std::hex << hdr->block[i]  << std::endl;
    frame->putLast(hdr->block[i]);
  }
 return frame;
}

void daemon_start(){
  pid_t pid;
  umask(0);

      if ((pid = fork()) < 0) {
        std::cerr << "Cannot fork! " << errno << std::endl;
        exit(2);

      }
      else if (pid) {
           exit(0);
      }

      if (setsid() < 0)
        exit(2);

      if (chdir("/home/romones/Univer/TLT-2346/CoAP/coap-repo") < 0 ){
        std::cout << "Cannot chdir! " << errno << std::endl;
        exit(2);
      }
     for (unsigned int i = 0; i < 1024; i++)
      {
        close(i);
      }

     int fd0 = open("/dev/null", O_RDONLY);
     int fd1 = open("/dev/null", O_RDWR);
     int fd2 = open("/dev/null", O_RDWR);

    if (fd0 != STDIN_FILENO || fd1 != STDOUT_FILENO || fd2 != STDERR_FILENO) {
      std::cout << "Unexpected File Descriptor\n";
      exit(2);
    }
}

string* parse_uri(string uri){

  string *retval = new string[5];

  string scheme = "none";
  string host  = "none";
  string port  = "none";
  string res  = "none";
  string query  = "none";

  pcrecpp::RE uri_re("(^coaps?)://([-a-zA-Z0-9._]+)(:[0-9]+)?([^:?#]+)?(\\?[-a-zA-Z0-9=&]+)?$");
  uri_re.PartialMatch(uri, &scheme, &host, &port, &res, &query);

  retval[0] = scheme;
  retval[1] = host;
  retval[2] = port;
  retval[3] = res;
  retval[4] = query;
  
 return retval;

}

Frame* pack_file (string filename){
  std::ofstream log ("./coap.log",std::fstream::app);
  using namespace std;
  ifstream::pos_type size;
  char * memblock;
  Frame* retval = new Frame ;


  ifstream file (filename.c_str(), ios::in|ios::binary|ios::ate);
    if (file.is_open()){
      //retval = new Frame;
      size = file.tellg();
       memblock = new char [size];
       file.seekg (0, ios::beg);
       file.read (memblock, size);
       file.close();

        log << timestamp() << "read " << size << "bytes\n" ;

       for (int i=0;i<((int)size-1);i++){
         retval->putLast(memblock[i]);        
       }
       log << timestamp() << "Frame is: " << retval->length() << "bytes\n";
    }
    else {
      log << timestamp() << "Unable to open file:" << filename << endl;
    }
   
    return retval ;
}

int save_file (Frame *data,string filename){
 using namespace std;

  ofstream::pos_type size;
  char block;
  size = data->length();
  int i;
  cout << "Size is: " << dec << size << "bytes\n";

  //memblock = new char[size];
  

  ofstream file(filename.c_str(), ios::out|ios::binary);
  if (file.is_open()){
    for (i = 0;i<(int)size;i++){
      block = data->getFirst();
      file.write (&block,1);
    }
    return i;
  }
  else  return 0;
}

int is_dir(string filename){
  int status;
  struct stat st_buf;

  status = stat (filename.c_str(), &st_buf);
  if (status){
    perror("ERROR: ");
    return -1;
  }

  if (S_ISREG(st_buf.st_mode)) {
    return 0;
  }
  if (S_ISDIR (st_buf.st_mode)) {
    return 1;
  }
  
}

int getdir (string path, std::vector<dirent *> &files){
   DIR *dp;
   struct dirent *dirp;
   struct dirent tmp;
      if((dp  = opendir(path.c_str())) == NULL) {
        std::cout << "Error(" << errno << ") opening " << path << std::endl;
       return errno;
      }

 while ((dirp = readdir(dp)) != NULL) {
   files.push_back(dirp);
 }
 closedir(dp);

 return 0;
}

string timestamp(){
    time_t ltime; 
    ltime=time(NULL);
    string retval = asctime( localtime(&ltime) );
    return retval.substr(0,retval.size()-1);
}

string read_code(Uint8 code){
  
  //code = htons(code);
  std::bitset<8> byte (code);
  std::bitset<3> class_(string("000"));
  std::bitset<5> detail(string("00000")); 
  
  for (int i = 0; i<5; i++){
    detail[i] =byte[i];
  }
  for (int i = 0; i<3; i++){
    class_[i] = byte[i+5];
   }
 
  if (code < 5 ){
   return codes_list[code];
  }
  
  std::ostringstream converter;
  converter << class_.to_ulong() << ".";
  converter << std::setw( 2 ) << std::setfill( '0' ) << detail.to_ulong(); 
  std::cout << byte << std::endl;
  std::cout << (int)code << std::endl;
  return converter.str();
}
