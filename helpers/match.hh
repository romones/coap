#ifndef MATCHTABLE_H
#define MATCHTABLE_H

#include <iostream>
#include <list>
#include <map>
#include <string>
#include <vector>
#include <ctime>
#include <doors/frame.h>
#include <doors/core.h>
#include <doors/ptb.h>
#include "options.hh"
#include "../impl/coappeer.hh"
#include "helpers.hh"

  enum Statustype
    {
      WAIT_ACK, READY, WAIT_RSP, DEAD
    };
  
  enum Matchtype
    {
      TOKENMATCH, IDMATCH, NONE
    };
  
  
  struct Entry_
  {
    Uint8 type;
    Uint8 code;
    Uint16 messageid;
    std::string token;//opt
    InetAddr targetaddress;//IP and PORT
    std::string uripath;//opt
    Uint16 contenttype; //opt
    Uint32 maxage;
    int retransmitctr;
    Statustype status;
    time_t timestamp;
    Frame data;
 
    Entry_() : token(""), uripath(""), contenttype(0), maxage(60){}
  };
 


class Matchtable 
{
 public:
   
   Matchtable(int max_size,int base, int ctr);
   ~Matchtable();
   
   void print();
   bool add(Entry_);
   
   void updatetable();
   void updatewithCON();
   void updatewithNON();
   void updatewithACK();
   void updatewithRST();
   //checks for local ip and port to match with destination and also for duplicates
   bool check_match_ack(CoapPeer::CMSG *msg);
   bool check_match_con(CoapPeer::CMSG *msg);
   std::vector<CoapPeer::CMSG> browsetable();
   void killzombies();
 private:
   
   unsigned int Listsize_;
  
   //this should point to last added Entry_ in the Entries
   unsigned int Current_;
   std::vector<Entry_> Entries;
   int rt_base;
   int rt_counter;
   
   
   Entry_ stripmessage(Message* msg);
   
   
};


#endif

