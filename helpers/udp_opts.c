#include <vector>
#include "helpers.hh"
#include "options.hh"


string docroot = "./";
string local_port = "5683";
string local_ip = "any";
string motd;
int rt_counter = 4;
int rt_timer = 15;


int parse_config (string filename){

  std::ifstream file (filename.c_str());
  if (!file.is_open()) {
     std::cerr << "Unable to open " << filename << std::endl;
     return 0;
  }

 pcrecpp::RE re("([A-Za-z_]+)=(.+)(#.+)?");
 string opt;
 string val;
 string comment;
 string line;

  while ( file.good() ){

   getline (file,line, '\n');
    if (line.length() == 0 ) continue;
    if (line.c_str()[0] == '#') continue;

   re.FullMatch(line,&opt, &val, &comment);
   //std::cout << opt << "\t" << val <<"\t" << comment << std::endl;

    if (opt == "DOCROOT") docroot = val;
    if (opt == "LOCAL_PORT") local_port = val;
    if (opt == "LOCAL_IP") local_ip = val;
    if (opt == "MOTD") motd = val;
    if (opt == "RT_COUNTER") rt_counter = atoi(val.c_str());
    if (opt == "RT_TIMER") rt_timer = atoi(val.c_str());
  }//while close

 return 1;
}

int main (int argc, char **argv){


bool daemon_flag = 0;
std::string config_file = "./coap.conf";

  char arg;
  opterr = 0;
  char delims[] = " ";

while ((arg = getopt (argc, argv, "dc:")) != -1){
  switch (arg) {
    case 'd':
      daemon_flag = 1;
      break;
    case 'c':
      config_file = strtok( optarg,delims);
      break;
    case '?':
      std::cerr << "Invalid arguments\n";
      exit(3);
  }

}


if (daemon_flag) std::cout << "Running as daemon\n";
  
  std::cout << "Config file:  " << config_file << std::endl;
   parse_config (config_file);

  std::cout << motd << std::endl;
  std::cout << "Local IP: " << local_ip;
  std::cout << "\nLocal Port: " << local_port;
  std::cout << "\nDocument Root: " << docroot;
  std::cout << "\nRetransmit counter: " << rt_counter;
  std::cout << "\nRetransmit timer: " << rt_timer << std::endl;
//   daemon_start();

string uri1 = "coap://example.com:6551/home/tmp/file1.txt?delete";
string uri2 = "coaps://192.168.0.1:6551/file1.txt?delete";
string uri3 = "coap://192.168.0.1/file1.txt?arg=0-a&arg1=1-b";
   
parse_uri(uri1);
parse_uri(uri2);
parse_uri(uri3);


HDRType Header;
HDRType *hptr = &Header;

Header.pack.ver = 1;
Header.pack.type = 3;
Header.pack.opcount = 3;
Header.pack.code = 7;
Header.pack.id = 97;

Frame *data;

HDRType *new_hdr = new HDRType;
data = pack_hdr(hptr);
new_hdr = unpack_hdr(data);
std::cout << std::dec << sizeof(*new_hdr) << std::endl;

Option* content_type;
Option* max_age;
Option* token;
Option* uri_path;


Uint32 cont = 12;
string tok = "0x1456";
string path = "/sensors/long_sensor_name/even_longer_name";

std::cout << "Make opts\n";

content_type = make_opt_int (CONTENT_TYPE,cont);
max_age = make_opt_int (MAX_AGE,16777210);
token = make_opt_string (TOKEN,tok);
uri_path = make_opt_string (URI_PATH, path);


std::cout << "Push to vector\n";
std::vector<Option> opt_vect;

opt_vect.push_back(*content_type);
opt_vect.push_back(*max_age);
opt_vect.push_back(*token);
opt_vect.push_back(*uri_path);

std::cout << "Options in vector: " << opt_vect.size() << std::endl;
 
std::cout << "Pack\n";
Frame *options_frame = new Frame;
options_frame = pack_opts(opt_vect);

std::cout << "New len is: " << options_frame->length() << std::endl;


std::cout << "Unpack\n";
std::vector<Option>* new_opts;
new_opts = unpack_opts(options_frame,4);


std::cout << "Read\n";
std::vector<Option>::iterator it;

 for ( it=new_opts->begin() ; it < new_opts->end(); it++ ){
  
  int dlt = it->header.pack.delta;

  switch (dlt){
    case CONTENT_TYPE:
    case MAX_AGE:
    case URI_PORT:
    case ACCEPT:
       std::cout << std::dec << read_opt_int(&(*it)) << std::endl;
       break;
    default:
       std::cout << read_opt_string(&(*it)) << std::endl;
       break;
  }

 }

pack_file("./hello.c");

/* 
// Create I/O Handler 
 IoHandler *io = IoHandler :: Instance ();

// Create main scheduler
 Scheduler *s = Scheduler :: Instance ();

// Create TimerTask
 TimerTask *tt = TimerTask :: instance (s, io);
         
 UiTask ui (s, io);

// Environment task
 EnvTask env (s);
 
// Scheduler for debugging
 DebugScheduler ds (0, 0, s);

 InetAddr addr(50000, "192.168.100.101");
 InetAddr own;

 own.set (50000);
 UdpTask udp("udp", &ds, io, &own);

 STATIC_SYMBOL (udp, 0);

UdpSAP :: User *usr = new UdpSAP :: User(&env, 0);
    env.add (usr, "usr");


while (ui.isTerminated () == false) {
      s->run ();
}

*/





 return 0;
}


