#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>

using namespace std;


/*function... might want it in some class?*/
int getdir (string dir, vector<dirent *> &files)
{
    DIR *dp;
    struct dirent *dirp;
    struct dirent tmp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(dirp);
    }
    closedir(dp);
    return 0;
}

int main()
{
    string dir = string("./files");
    vector<dirent *> files = vector<dirent *>();

    getdir(dir,files);

    for (unsigned int i = 0;i < files.size();i++) {
        cout << files[i]->d_name << endl;
        int tmp = files[i]->d_type;
        cout << tmp << endl;
    }
    return 0;
}
