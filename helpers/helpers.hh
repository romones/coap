#include <pcrecpp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <doors/ptb.h>
#include <doors/lemon.h>
#include <doors/udp.h>
#include <doors/udptask.h>
#include <doors/inetaddr.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <bitset>
#include <vector>
#include <string>
#include <time.h>

#ifdef __sparc
#include "/usr/local/gnu/stow/file-4.20/include/magic.h"
#elif __linux__
#include <magic.h>
#endif

#ifndef _HELPERS_HH_
#define _HELPERS_HH_


#define Ver 1
#define CON 0
#define NON 1
#define ACK 2
#define RST 3

#define GET 1
#define POST 2
#define PUT 3
#define DELETE 4


const std::string types_list[] = {"CON","NON","ACK","RST"};
const std::string codes_list[] = {"EMPTY","GET","POST","PUT","DELETE"};


struct HDRStruct{
///*#ifdef __sparc
  unsigned int id :16;
  unsigned int code :8;
  unsigned int opcount :4;
  unsigned int type :2;
  unsigned int ver :2;

/*#elif defined __linux__ 
  unsigned int ver :2;
  unsigned int type :2;
  unsigned int opcount :4;
  unsigned int code :8;
  unsigned int id :16;
//#endif */
};

union HDRType {
  HDRStruct pack;
  Uint32 bytes;
  Uint8 block[4];
};

HDRType* unpack_hdr(Frame *frame);

Frame* pack_hdr(HDRType *hdr);


void daemon_start();

string* parse_uri(string uri);

Frame* pack_file (string filename);

int save_file(Frame *data,string filename);

int is_dir(string filename);

int getdir (string path, std::vector<dirent *> &files);

string timestamp();

string read_code(Uint8 code);


#endif


