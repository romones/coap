
#include "cache.hh"

Cache::Cache(){
  table.erase(table.begin(),table.end());
}

Cache::~Cache(){
  table.erase(table.begin(),table.end());
}

void Cache::add(CacheEnt target){
  table.push_back(target);
  std::cout << target.path << " <---\n";
}

void Cache::complete(Uint16 msgid, Uint32 expires, Frame payload ){
  
 for (int i=0 ; i < table.size(); i++ ){
  if (table[i].msgid = msgid){
    table[i].expires = expires;
    table[i].payload = payload.copy();
    table[i].status = FULL;
  }
 }
}

void Cache::remove(int index){
  table.erase(table.begin()+index);
}

void Cache::freshen (int index, int time){
  table[index].expires = time;
}

int Cache::match(CacheEnt target){
 
 for (int i=0 ; i < table.size(); i++ ){
    if (table[i].from.addrToString() == target.from.addrToString()){
      if (table[i].host == target.host){
        if (table[i].path == target.path){
          if (table[i].query == target.query){
            return i;
          }
          std::cout << "miss path\n";
        }
        std::cout << "miss host\n";
      }
      std::cout << "miss from\n";
    }
 }//for close

 return MISS;
}

CacheEnt Cache::get_by_id(int index){
  return table[index];
}
  
void Cache::print(){

 for (int i=0 ; i < table.size(); i++ ){
  std::cout << i << " " << table[i].from.addrToString() << " ";
  std::cout << table[i].msgid << " " << table[i].status << " ";
  std::cout << table[i].type << " ";
  std::cout << table[i].expires << " " << table[i].cont_type << " ";
  std::cout << table[i].host << " ";
  if (table[i].path != "" ) std::cout << table[i].path << " ";
  else std::cout << "no_path ";
  std::cout << table[i].query << " ";
  std::cout << table[i].payload.length() << "bytes\n";
 }
}





