#include "options.hh"


Frame* pack_opts(std::vector<Option> &array){
    
    Frame* retval = new Frame;
    
    //printf("Delta is: %d\n", array[0].header.pack.delta);
    //printf("Length is: %d\n", array[0].header.pack.length);
    //printf("Frame is: %d\n", array[0].value.length());
    
    retval->putLast(array[0].header.byte);
    retval->combine(array[0].value);
    //std::cout<<"\n";

    for (int i=1;i<array.size();i++){
      array[i].header.pack.delta -= array[i-1].header.pack.delta;
     // printf("Delta is: %d\n", array[i].header.pack.delta);
     // printf("Length is: %d\n", array[i].header.pack.length);
     // printf("Frame is: %d\n", array[i].value.length());
     // std::cout<<"\n";
      
      retval->putLast(array[i].header.byte);
      retval->combine(array[i].value);
  
      std::cout<<"\n";
    }
    
      std::cout << std::dec << "Result length: ";
      std::cout << retval->length() << std::endl;
  return retval;

}

std::vector<Option>* unpack_opts(Frame* target, int count){
 
  std::cout << "Unpacking Options\n";

  std::vector<Option> *retval = new std::vector<Option>;
  Option *iterator = NULL;
  int saved_delta =0;
  int length;

  iterator = new Option;
  
  for (int i=0; i<count; i++){
    iterator = new Option;
    iterator->header.byte = target->getFirst();

    int tmp = iterator->header.pack.delta;
    iterator->header.pack.delta += saved_delta;
    saved_delta = tmp;

    length = iterator->header.pack.length;

    if (length == 15){
      length = 16 + (Uint8)target->read(0);
      //printf ("byte is %d, total is %d", (Uint8)target->read(0),length);
    }
    

    //printf ("Delta is: %d\n", iterator->header.pack.delta);
    //printf ("Length is: %d\n", iterator->header.pack.length);
  
    iterator->value = target->split(length);
    retval->push_back(*iterator);
  
    delete iterator;
    iterator = NULL;
  }
 
  //std::cout << "Remains in frame:" << target->length() << std::endl;
  return retval;
}

Option* make_opt_string(Uint8 delta, string value){
 
   //std::cout << "In make_opt_string\n";
   //std::cout << value << std::endl;

   if (delta > 15) { std::cout << "Invalid delta\n"; return NULL;}
   if (sizeof(value) > 270) { std::cout << "Value too big\n"; return NULL;}

   Option* retval = new Option;
   
   retval->header.pack.delta = delta;
   Frame tmp;
   for (int i = 0;i<value.length(); i++){
         tmp.putLast((value.c_str()[i]));
   }

   if (value.length() > 15){
     Uint8 diff = value.length() -15;
     std::cout << "length more than 15\n";
      retval->header.pack.length = 15;
      retval->value.putLast(diff);
      retval->value.combine(tmp);
   }
   else {
      retval->header.pack.length = value.length();
      retval->value = tmp;
   } 
 std::cout << "Value length is:" << std::dec << retval->value.length() << std::endl;
 return retval;

}

string read_opt_string(Option* target){
  
  std::cout << "Reading option" << std::endl;
  int length;
  if ( target->header.pack.length == 15) {

    std::cout << "15 length detected\n";
    length = target->value.getFirst();
    length += 15;
  }
  else {
    length = target->header.pack.length;
  }

  //std::cout << "Length is: " << length << std::endl;
  string retval = "";
  for (int i = 0; i<length;i++){
    retval += target->value.read(i);
  }
 return retval;
}

Option* make_opt_int(Uint8 delta,Uint32 value){
 
   //std::cout << "In make_opt_int\n";

   if (delta > 16) { std::cout << "Invalid delta\n"; return NULL;}

   int_bytes tmp;
   tmp.val = value;
   //std::cout << std::dec << tmp.val << std::endl;

   int length = 0;
   
   for (int i = 3;i>=0;i--){ 
    if (tmp.block[i] != 0) {
     length = i+1;
     break;
    } 
    //std::cout << "Byte" << i;
    //printf ("%x\n",tmp.block[i]);
   }
   
  
   Option* retval = new Option;
   
   retval->header.pack.delta = delta;
   retval->header.pack.length = length;
   for (int i = 0;i<length; i++){
     //printf ("%x\n",tmp.block[i]);
     retval->value.putLast(tmp.block[i]);
   }

  std::cout <<std::dec << "Value length is:" << retval->value.length() << std::endl;
 return retval;

}

Uint32 read_opt_int(Option* target){
  std::cout << "Reading option int \n";
  int_bytes tmp;
  tmp.val = 0;
  
  int length = target->header.pack.length;
   for (int i=0; i<length;i++){
    tmp.block[i] = target->value.read(i); 
   }
  return tmp.val;
}

std::vector<Option>* assemble_opts (   string URI, Uint32 MaxAge, string Token,  Uint16 Content_Type ){
 
 std::vector<Option>* opt_vect = new std::vector<Option>;
 string *uri_block = parse_uri (URI);

  if (Content_Type){
   std::cout << "Adding Content-Type\n";
   Option* content_type;
    content_type = make_opt_int(CONTENT_TYPE,Content_Type);
    opt_vect->push_back(*content_type);
   delete content_type;
   content_type = NULL;
  }

  //if there is MaxAge set it
  if (MaxAge != 0) {
   std::cout << "Adding MaxAge\n";
   Option* max_age;
    max_age = make_opt_int(MAX_AGE,MaxAge);
    opt_vect->push_back(*max_age);
   delete max_age;
   max_age = NULL;
  }

  //add URI-host
  std::cout << "Adding URI-Host\n";
  Option* uri_host;
    uri_host = make_opt_string(URI_HOST,uri_block[1]);
    opt_vect->push_back(*uri_host);
  delete uri_host;
  uri_host = NULL;

  //add URI-Port
  std::cout << "Adding URI-Port\n";
  Option* uri_port;
    uri_port = make_opt_int(URI_PORT,atoi(uri_block[2].substr(1).c_str()));
    opt_vect->push_back(*uri_port);
  delete uri_port;
  uri_port = NULL;

   
  //add URI-Path
  if (uri_block[3] != ""){
  std::cout << "Adding URI-Path\n";
  std::cout << uri_block[3] << std::endl;
  Option* uri_path;
  uri_path = make_opt_string(URI_PATH,uri_block[3]);
  opt_vect->push_back(*uri_path);
  delete uri_path;
  uri_path = NULL;
  }

  //add URI-Query
  if (uri_block[4] != "" ){
   Option* uri_query;
    uri_query = make_opt_string(URI_QUERY,uri_block[4]);
    opt_vect->push_back(*uri_query);
  delete uri_query;
  uri_query = NULL;
  }



  //If Token is provided, set it
  
  if (Token != ""){
   std::cout << "Adding Token\n";
   Option *token;
    token = make_opt_string(TOKEN,Token);
    opt_vect->push_back(*token);
   delete token;
   token = NULL;
  }
  
  // For now these are all options supported

 return opt_vect;
}

