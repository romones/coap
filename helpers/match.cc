#include "match.hh"

Matchtable::Matchtable (int max_size, int base , int ctr){

  Listsize_ = max_size;
  Current_ = 0;
  rt_base = base;
  rt_counter = ctr;
}


Matchtable::~Matchtable(){}


void Matchtable::print(){
 
 if (Entries.size() == 0 ) return;
 std::cout << "Matchtable:\n";
 for (Current_ = 0; Current_ < Entries.size(); Current_++ ){
   std::cout << Entries.at(Current_).type << "\t";
   std::cout << Entries.at(Current_).code << "\t";
   std::cout << Entries.at(Current_).messageid << "\t";
   std::cout << Entries.at(Current_).token << "\t";  
   std::cout << Entries.at(Current_).targetaddress.addrToString() << "\t"; 
   std::cout << Entries.at(Current_).uripath << "\t"; 
   std::cout << Entries.at(Current_).retransmitctr << "\t"; 
   std::cout << Entries.at(Current_).status << "\t"; 
   std::cout << Entries.at(Current_).timestamp << "\t"; 
   std::cout << Entries.at(Current_).data.length() << "bytes" << "\t"; 
   std::cout << std::endl;
 }
 std::cout << "End of Matchtable\n";

}


Entry_ Matchtable::stripmessage(Message* msg)
{
   Entry_ temp;
   
   //default values
   temp.type = 0;
   temp.code = 0;
   temp.messageid = 0;
   temp.token = "";
   temp.targetaddress = InetAddr();
   temp.uripath = "";
   temp.retransmitctr = 0;
   temp.status = DEAD;
   //temp.ackto = NULL;
   temp.timestamp = (time(0)+4);
   //temp.whole = msg;
   temp.retransmitctr = 0 ;
   
   Frame hdr = ((CoapPeer::CMSG*) msg)->Header;
   HDRType* headers = new HDRType;
   headers = unpack_hdr(&hdr);
   
   temp.type = headers->pack.type;
   temp.code = headers->pack.code;
   temp.messageid = headers->pack.id;
   
   Frame opts = ((CoapPeer::CMSG*) msg)->Options;
   std::vector<Option>* temp2;
   int count = headers->pack.opcount;
   temp2 = unpack_opts(&opts, count);
   
   
   unsigned int deltasum = 0;
   for (int i = 0; i < count; i++)
   {
      deltasum =+ (temp2->at(i)).header.pack.delta;
      
      if (deltasum == 5)
      {
      temp.targetaddress.set(((CoapPeer::CMSG*) msg)->addr);
      }
      if (deltasum == 11)
      {
	 temp.token = read_opt_string(&(temp2->at(i)));
      }
      if (deltasum == 9)
      {
      temp.uripath = read_opt_string(&(temp2->at(i)));
      }
   }
   return temp; 
}


bool Matchtable::add(Entry_ target){
  Entries.push_back(target);   
  return 1;
}


void Matchtable::updatetable()
{
   switch (Entries.at(Current_).type)
   {
    case 0:
      updatewithCON();
    case 1:
      updatewithNON();
    case 2:
      updatewithACK();
    case 3:
      updatewithRST();
      break;
   }
   
   return;   
}

void Matchtable::updatewithCON()
{
   for(unsigned int i = 0; i < Entries.size(); ++i)
   {
      if(Entries.at(i).status != DEAD)
      {
         //if tokens match
         if (i != Current_ && Entries.at(i).token == Entries.at(Current_).token)
         {
            Entries[i].status = READY;
         }  
      }
   }
   Entries.at(Current_).status = WAIT_ACK;
}

void Matchtable::updatewithNON(){}

void Matchtable::updatewithACK(){}
void Matchtable::updatewithRST(){}

bool Matchtable::check_match_ack(CoapPeer::CMSG *msg)
{
  std::ofstream log ("./coap.log",std::fstream::app);
  log << timestamp() << " Matching... ";
    Message* msg_copy = msg->clone();

     HDRType* msgHdr = unpack_hdr(&(((CoapPeer::CMSG *)msg_copy)->Header));
      
     for(unsigned int i = 0; i < Entries.size() ; ++i)
     {    
         if (Entries.at(i).targetaddress.addrToString() ==
             ((CoapPeer::CMSG *)msg_copy)->addr.addrToString())
         {
         if (msgHdr->pack.id == Entries.at(i).messageid){
            log << "Found ID ";
         
             if (Entries.at(i).status == WAIT_ACK){
              log << "that we were waiting for ";
                if (msgHdr->pack.code !=0 && 
                  ((CoapPeer::CMSG *)msg_copy)->Payload.length() !=0 ){
                     log << "and it's not empty\n";
                     Entries.at(i).status = DEAD;
                     return true;
                }//close if not empty
                else {
                log << "but it's empty\n";
                Entries.at(i).status = WAIT_RSP;
                return false;
                }//close if empty
             }//close id WAIT_ACK
             else {
              log << "seems Duplicate\n";
              return false;
             }//close if not WAIT_ACK
         }//close if ID match
         }//close if IP match
     }//for close

}
 
bool Matchtable::check_match_con(CoapPeer::CMSG *msg){
  std::ofstream log ("./coap.log",std::fstream::app);
  log << timestamp() << " Matching... ";
    Message* msg_copy = msg->clone();

     HDRType* msgHdr = unpack_hdr(&(((CoapPeer::CMSG *)msg_copy)->Header));

    std::vector<Option>* opt_vect = unpack_opts(&(((CoapPeer::CMSG *)msg_copy)->Options),
                                                msgHdr->pack.opcount);

     std::string token = "";
     for (int i =0; i < opt_vect->size();i++){
        if (opt_vect->at(i).header.pack.delta == 11 ){
          token = read_opt_string(&(opt_vect->at(i)));
        }
     
     }
      
     for(unsigned int i = 0; i < Entries.size() ; ++i)
     {    
       if (Entries.at(i).token == token ){
         log << " token matched ";           
           
           if (Entries.at(i).targetaddress.addrToString() ==
             ((CoapPeer::CMSG *)msg_copy)->addr.addrToString()){
               
               log << "and ip matched\n";
               Entries.at(i).status = DEAD;
               return true;
           }// if ip match close
           else{
               log << "but ip doesn't match\n";
               return false;
           }//if ip doesn't match close

       }
     }//for close
     return false;
}

std::vector<CoapPeer::CMSG> Matchtable::browsetable()
{
   
   std::vector<CoapPeer::CMSG> messages4rtr;
   
   for( size_t i = 0; i < Entries.size(); i++)
     {
  

	if ( (Entries.at(i).status != DEAD && Entries.at(i).status != READY && Entries.at(i).status != WAIT_RSP) 
	     && (Entries.at(i).retransmitctr > 0) 
	     && (time(NULL) > Entries.at(i).timestamp) )
	  {
	     Entries.at(i).timestamp = time(NULL) + rt_base;
	     Entries.at(i).retransmitctr--;
	     
	     CoapPeer::CMSG *retrans = new CoapPeer::CMSG; // *) peer.create (CoapPeer::cmsg);
	     HDRType re_hdr;
	     re_hdr.pack.ver = 1;
	     re_hdr.pack.type = Entries.at(i).type;
	     re_hdr.pack.code = Entries.at(i).code;
	     re_hdr.pack.id = Entries.at(i).messageid;
	     
	     Uint8 delta = 0;
       Option *target;
	     
	     std::vector<Option> opts;
	     //content type? max-age? 
	     
	     //1contenttype
	     if( Entries.at(i).contenttype != 0)
		 {
		    delta = 1-delta;
        target = make_opt_int(delta,Entries.at(i).contenttype);
		    opts.push_back(*target);
		 }
	     if( Entries.at(i).maxage != 60)
	       {
		  delta = 2-delta;
      target = make_opt_int(delta, Entries.at(i).maxage);
		  opts.push_back(*target);
	       }
	     
	     
	     //5urihost
	     delta = 5-delta;
	     string temp_host 
	       = Entries.at(i).targetaddress.addrToString().substr(0,
		 Entries.at(i).targetaddress.addrToString().find(":"));
       target = make_opt_string(delta, temp_host);
	     opts.push_back(*target);
	     
	     //7uriport
	     delta = 7-delta;
	     //int port = static_cast<int>(Entries.at(i).targetaddress.addrToString().erase(0,
		   //Entries.at(i).targetaddress.addrToString().find(":")));
       Uint16 port = Entries.at(i).targetaddress.getPortNumber();
       target = make_opt_int(delta, port);
	     opts.push_back(*target);
	     
	     //9uripath
       
	     if ( Entries.at(i).uripath != "")
	       {
          delta = 9 - delta;
          target = make_opt_string(delta, Entries.at(i).uripath); 
		      opts.push_back(*target);
	       }
	     
	     
	     //11token
	     if ( Entries.at(i).token != "")
	       {
		      delta = 11-delta;
          target = make_opt_string(delta, Entries.at(i).token);
		      opts.push_back(*target);
	       }
	     
	     //11uripath - delta values question, see soon
       //target = make_opt_string(delta, Entries.at(i).token);
	     //opts.push_back(*target);
	     
	     
	     re_hdr.pack.opcount = opts.size();
       Frame* hptr = pack_hdr(&re_hdr);
       Frame* tmp =  pack_opts(opts);
       retrans->addr.set(Entries.at(i).targetaddress);
       retrans->Header = *hptr;
	     retrans->Options = *tmp;
					 
	     retrans->Payload = Entries.at(i).data;
	     
	     // message should be ready for wire
	     messages4rtr.push_back(*retrans);
	  }
	if ( Entries.at(i).retransmitctr == 0 )
	  {
	     Entries.at(i).status = DEAD;
	  }
	
     } // for
   
   return messages4rtr;
}

void Matchtable::killzombies()
{
   size_t i = 0;
   unsigned int counter = 0;
   while( i < Entries.size())
     {
	if(Entries.at(i).status==DEAD)
	  {
	     std::vector<Entry_>::iterator it = (Entries.begin() + i);
	     
	     Entries.erase(it);
	     counter++;
	  }
	else
	  {
	     i++;
	  }
	
     }
  // std::cerr << "!!! " << counter << " entries deleted, " << Entries.size()
  //   << " left" << std::endl;
}





/*
//   if(own->addrToString() == msg->addr.addrToString())
//   {
     std::cout << "Address match\n";

     HDRType* msgHdr = unpack_hdr(&(msg->Header));
      for(unsigned int i = 0; i < Entries.size() ; ++i)
      {
         if (msgHdr->pack.id == Entries.at(i).messageid && 
             msgHdr->pack.type == (unsigned int)Entries.at(i).type)
         {
            //duplicate
            return false;
         }  
      }
      return true;
//   }
//   else
//   {
      //not for us
//      return false;
//   }   
*/
//}



