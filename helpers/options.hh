#include <iostream>
#include <string>
#include <fstream>
#include <pcrecpp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <doors/ptb.h>
#include <doors/lemon.h>
#include <doors/udp.h>
#include <doors/udptask.h>
#include <doors/inetaddr.h>
#include <bitset>
#include <string>
#include <iostream>
#include <vector>
#include "helpers.hh"


#ifndef _OPTIONS_HH_
#define _OPTIONS_HH_


 #define   CONTENT_TYPE  1    //int
 #define   MAX_AGE  2         //int
 #define   PROXY_URI  3
 #define   ETAG  4
 #define   URI_HOST  5
 #define   LOCATION_PATH  6
 #define   URI_PORT  7        //int
 #define   LOCATION_QUERY  8
 #define   URI_PATH  9
 #define   OBSERVE  10
 #define   TOKEN  11
 #define   ACCEPT  12         //int
 #define   IF_MATCH  13
 #define   MAX_OFE  14
 #define   URI_QUERY  15
 #define   IF_NONE_MATCH  21

#define TEXT_PLAIN 0
#define APPLICATION 40

//Data type used to access a long int byte by byte
union int_bytes{
  Uint32 val;
  Uint8 block[4];
};

//Bitfield to reflect option header.
struct OPTStruct{

#ifdef __sparc
  Uint8 delta :4;
  Uint8 length :4;
#elif defined __linux__
  Uint8 length :4;
  Uint8 delta :4;
#endif  
};

//Data type for option header alowing access to the header in ine byte
union OPTByte{
  OPTStruct pack;
  Uint8 byte;
};

//Main data type for Option entity
struct Option {
  OPTByte header;
  Frame value;
};


//The function to assemble all required options into one 
//big frame. The resulting frame is ready to be used in PDU
Frame* pack_opts(std::vector<Option> &array);

//Decode a frame from PDU into an array of options.
std::vector<Option>* unpack_opts(Frame* target, int count);

//Function returns an Option Entity given options number, and string value
Option* make_opt_string(Uint8 delta, string value);

//Extract value from a string based Option. The option itself
//remains unchanged.
string read_opt_string(Option* target);

//Function returns an Option Entity given options number, and int value
//for proper work, the value should be cast to Uint32 type before using
//in function.
Option* make_opt_int(Uint8 delta,Uint32 value);

//Read value of an int based option. The option itself remains 
//unmodified.
Uint32 read_opt_int(Option* target);

std::vector<Option>* assemble_opts (   string URI, Uint32 MaxAge, string Token,  Uint16 Content_Type );
 

#endif
