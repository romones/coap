#ifndef _cache_hh_
#define _cache_hh_

#include "helpers.hh"
//cache match returns

#define MISS -1 

#define PENDING 0
#define FULL 1


struct CacheEnt{
  InetAddr from;
  Uint16 msgid;
  int status;
  int type;
  Uint32 expires;
  Uint16 cont_type;
  string host;
  string path;
  string query;
  Frame payload;
};

class Cache{
 private:
  std::vector<CacheEnt> table;
  
 public:
  Cache();
  ~Cache();
  void add(CacheEnt target);
  void complete(Uint16 msgid, Uint32 expires, Frame payload );
  void remove(int index);
  int match(CacheEnt target);
  void freshen (int index, int time);
  CacheEnt get_by_id(int index);
  
  void print();

};

#endif
