INCLUDE=-I/opt/doors/include/
BUILDDIR=./build
DOORS=/opt/doors/lib/libdoors_tools.so /opt/doors/lib/libdoors_base.so /opt/doors/lib/libdoors_protocols.so

coap: main.o coapsap.o coapprotofsm.o coapappfsm.o cache.o match.o\
		coapprototask.o coapapptask.o coappeer.o helpers.o options.o 
	gcc  $(DOORS) /usr/lib64/libstdc++.so.6 /usr/lib64/libpcrecpp.so\
		/usr/lib64/libmagic.so.1.0.0\
		main.o\
		coapsap.o\
		coapprotofsm.o\
		coapappfsm.o\
		coapprototask.o\
		coapapptask.o\
		options.o\
		helpers.o\
		coappeer.o\
		cache.o\
		match.o\
		-o coap
coap-32: main.o coapsap.o coapprotofsm.o coapappfsm.o cache.o match.o\
		coapprototask.o coapapptask.o coappeer.o helpers.o options.o 
	gcc $(DOORS) -m32 /usr/lib64/libstdc++.so.6 /usr/lib64/libpcrecpp.so\
		/usr/lib64/libmagic.so.1.0.0\
		main.o\
		coapsap.o\
		coapprotofsm.o\
		coapappfsm.o\
		coapprototask.o\
		coapapptask.o\
		options.o\
		helpers.o\
		coappeer.o\
		cache.o\
		match.o\
		-o coap
main.o:	./impl/main.C
	g++ -c -lpcrecpp $(INCLUDE)  ./impl/main.C -o main.o
coapsap.o: interface/coapsap.cc interface/coapsap.hh
	g++ -c $(INCLUDE) interface/coapsap.cc -o coapsap.o
coapprotofsm.o:	./impl/coapprotofsm.sm ./impl/coapprotofsm.cc ./impl/coapprotofsm.hh
	g++ -c -lpcrecpp $(INCLUDE)  ./impl/coapprotofsm.cc -o coapprotofsm.o
coapappfsm.o:	./impl/coapappfsm.sm ./impl/coapappfsm.cc ./impl/coapappfsm.hh
	g++ -c -lpcrecpp $(INCLUDE)  ./impl/coapappfsm.cc -o coapappfsm.o
coapprototask.o:	./impl/coapprototask.hh ./impl/coapprototask.C
	g++ -c -lpcrecpp $(INCLUDE)  ./impl/coapprototask.C -o coapprototask.o
coapapptask.o:	./impl/coapapptask.hh ./impl/coapapptask.C
	g++ -c -lpcrecpp -lmagic $(INCLUDE)  ./impl/coapapptask.C -o coapapptask.o
coappeer.o: ./impl/coappeer.pdu ./impl/coappeer.i ./impl/coappeer.hh ./impl/coappeer.C
	g++ -c -lpcrecpp $(INCLUDE)  ./impl/coappeer.C -o coappeer.o 
helpers.o: ./helpers/helpers.hh ./helpers/helpers.cc
	g++ -c $(INCLUDE) ./helpers/helpers.cc -o helpers.o
cache.o: ./helpers/cache.hh ./helpers/cache.cc
	g++ -c $(INCLUDE) ./helpers/cache.cc -o cache.o
match.o: ./helpers/match.hh ./helpers/match.cc
	g++ -c $(INCLUDE) ./helpers/match.cc -o match.o
options.o: ./helpers/options.hh ./helpers/options.cc
	g++ -c $(INCLUDE) ./helpers/options.cc -o options.o
recvr: main.o coapsap.o coapprotofsm.o coapprototask.o coappeer.o helpers.o options.o
	gcc $(DOORS) /usr/lib64/libstdc++.so.6 /usr/lib64/libpcrecpp.so main.o coapsap.o coapprotofsm.o coapprototask.o options.o coappeer.o helpers.o -o recvr
clean:
	rm -vf ./*.o

