#ifndef _coapapptask_h_
#define _coapapptask_h_

#include <doors/ptb.h>
#include <doors/phys.h>
#include <doors/ptask.h>
#include <doors/timer.h>
#include <doors/hsi.h>
#include <doors/udp.h>
#include "../interface/coapsap.hh"
#include "coappeer.hh"

class CoapAppFSM;

class CoapAppTask : public PTask {
  public:
   CoapAppTask ( std::string name, Scheduler *s, CoapAppFSM *sm, std::string docroot);
   ~CoapAppTask (void);

   CoapSAP::User down;
   CoapSAP::Provider up;

   Timer app_timer_;
   std::string base;

   //Main Satate
   //
   bool process_req(Message* m);
   bool process_result(Message* m);
   bool default_app(Message* m);
   bool test_proto(Message* m);
};

#endif
