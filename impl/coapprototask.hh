#ifndef _coapprototask_h_
#define _coapprototask_h_

#include <doors/ptb.h>
#include <doors/phys.h>
#include <doors/ptask.h>
#include <doors/timer.h>
#include <doors/hsi.h>
#include <doors/udp.h>
#include <fstream>
#include <string>
#include "../interface/coapsap.hh"
#include "../helpers/helpers.hh"
#include "../helpers/cache.hh"
#include "../helpers/match.hh"
#include "coappeer.hh"

class CoapProtoFSM;

class CoapProtoTask : public PTask {
  public:
   CoapProtoTask ( std::string name, Scheduler *s, CoapProtoFSM *sm, int rt_count, int rt_timer, InetAddr local );
   ~CoapProtoTask (void);

   UdpSAP::User down;
   CoapSAP::Provider up;
   CoapPeer peer;

   Timer piggy_timer;
   Timer retransmit_timer;
   Uint16 mess_id;
   Uint16 pending_id;
   int rt_counter; 
   InetAddr pending_addr;
   std::ofstream log;

   bool piggy_flag;
   bool retransmit_flag;

   InetAddr own;

   Cache cache;
   Matchtable mtable;
 
   //Main Satate
   //
   bool process_app_req(Message* m);
   bool process_peer_msg(Message* m);
   bool default_action(Message* m);
   bool test_send (Message* m);
   bool piggyback_req(Message *msg);
   bool empty_ack_send(Message *msg);
   bool timer_mux(Message *msg);
};

bool check_match(CoapPeer::CMSG *msg);
#endif
